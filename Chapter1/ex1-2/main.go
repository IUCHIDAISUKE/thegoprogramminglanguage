package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("Program Name :", os.Args[0])
	for i, arg := range os.Args[1:] {
		fmt.Println("Index :", i+1, "Argument :", arg)
	}
}
