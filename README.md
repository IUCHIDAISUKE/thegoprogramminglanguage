# The Go Programming Language

## Execution environment

```shell
Mac OS :Big Sur
$ go version
go version go1.15.5 darwin/amd64
```

## Compile

```shell
# Compile and run
$go run main.go

# Compile and name of the executable is test.
$go build -o test main.go
```
